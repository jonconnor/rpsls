

const commence = document.getElementById("button");

commence.addEventListener ("click", function(){

  let answers = ["rock", "paper", "scissors", "lizard", "Spock"];

  function randomValue() {
    return answers[Math.floor(Math.random() * answers.length)];
  }

  let ai = randomValue();

  let hero = randomValue();

  document.getElementById("hero").innerHTML = hero;

  document.getElementById("villan").innerHTML = ai;

  let heroRockWin = hero === "rock" && (ai === "scissors"|| ai === "lizard")

  let heroPaperWin = hero === "paper" && (ai === "rock"|| ai === "Spock")

  let heroScissorsWin = hero === "scissors" && (ai === "paper"|| ai === "lizard")

  let heroLizardWin = hero === "lizard" && (ai === "paper"|| ai === "Spock")

  let heroSpockWin = hero === "Spock" && (ai === "scissors"|| ai === "rock")

  if (hero === ai) {
    console.log('tis a draw');
    document.getElementById("result").innerHTML = '\'tis a draw...';
  } else if (heroRockWin || heroPaperWin || heroScissorsWin || heroLizardWin || heroSpockWin) {
    console.log("victory to the hero");
    document.getElementById("result").innerHTML = 'Victory to the Hero\!';
  } else {
    console.log('the machine has conquered');
    document.getElementById("result").innerHTML = 'Victory to the Villan\!';
  }
});
